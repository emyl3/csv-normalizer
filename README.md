# csv-normalizer
The purpose of the csv-normalizer is to take a CSV file at the provided file path and output a new normalized CSV file at the provided file path.

"Normalized" here means:
- UTF-8 character set
- Timestamp is in ISO-8601 format
- ZIP codes are 5 digits
- FullName is in uppercase
- Address and Notes are Unicode validated
- FooDuration, BarDuration, TotalDuration are in seconds
- Invalid UTF-8 characters are replaced with the Unicode Replacement Character

Sample CSVs and their normalized results are in the `./data` directory.

## Assumptions
- The provided CSV contains a header with the following columns:
  - Timestamp
  - Address
  - ZIP
  - FullName
  - FooDuration
  - BarDuration
  - TotalDuration
  - Notes

## Setup
### Prerequisite
- `node` and `npm` installed
- run on macOS 10.13

### Installation
- clone this repo
- in your local version of this repo, run `npm ci`

## Development
- To run linter: `npm run lint`
- To run test: `npm run test`

## Usage
- To normalize a CSV file, run `npm start <csv_input_file_path> <csv_output_file_path>`
